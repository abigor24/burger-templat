import React from 'react';
import './Burger.css';
import Ingredient from "./Ingredient/Ingredient";

const Burger = props => {
  const ingredientKeys = Object.keys(props.ingredients);
  let ingredients = [];

  ingredientKeys.forEach(ingredient => {

    let amount = props.ingredients[ingredient];

    for (let i = 0; i < amount; i++) {
      ingredients.push(<Ingredient type={ingredient}/>)
    }
  });

  return (
      <div className="Burger">
        <Ingredient type="bread-top"/>
        {ingredients}
        <Ingredient type="bread-bottom"/>
      </div>
  );
};

export default Burger;
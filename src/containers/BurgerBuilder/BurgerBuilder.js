import React, {Component} from 'react';
import Wrapper from "../../hoc/Wrapper";
import Burger from "../../components/Burger/Burger";

class BurgerBuilder extends Component {
  state = {
    ingredients: {
      bacon: 5,
      salad: 1,
      cheese: 2,
      meat: 1
    }
  };

  render() {
    return (
      <Wrapper>
        <Burger ingredients={this.state.ingredients}/>
        <div>Build controls will be here</div>
      </Wrapper>
    )
  }
}

export default BurgerBuilder;